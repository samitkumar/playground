package tutorial.service;

import javax.jws.WebService;

import tutorial.model.ServiceRequest;
import tutorial.model.ServiceResponse;

/**
 * The Class ServiceImpl.
 */
@WebService(
		name ="MyService",
		portName = "MyServicePort",
		serviceName = "MyServiceService",
		endpointInterface = "tutorial.service.ServiceInterface"
		)
public class ServiceImpl implements ServiceInterface {
	
	/* (non-Javadoc)
	 * @see tutorial.service.ServiceInterface#getMyServiceData(tutorial.model.ServiceRequest)
	 */
	@Override
	public ServiceResponse getMyServiceData(ServiceRequest serviceRequest) throws ServiceException {
		ServiceResponse serviceResponse = new ServiceResponse();
		serviceResponse.setId(serviceRequest.getId());
		serviceResponse.setDesignation("ABCD");
		serviceResponse.setName(serviceRequest.getName());
		serviceResponse.setSalary(0.0f);
		serviceResponse.setError("NO_ERROR");
		return serviceResponse;
	}

	/* (non-Javadoc)
	 * @see tutorial.service.ServiceInterface#ping(java.lang.String)
	 */
	@Override
	public String ping(String pingRequest) throws ServiceException {
		if(pingRequest!=null && !pingRequest.isEmpty()){
			return pingRequest;
		}
		return "pong";
	}

}

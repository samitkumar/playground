package tutorial.service;

import javax.jws.WebService;

import tutorial.model.ServiceRequest;
import tutorial.model.ServiceResponse;

/**
 * The Interface ServiceInterface.
 */
@WebService(targetNamespace = "http://tutorial.com")
public interface ServiceInterface {
	
	/**
	 * Gets the my service data.
	 *
	 * @param serviceRequest the service request
	 * @return the my service data
	 * @throws ServiceException the service exception
	 */
	public ServiceResponse getMyServiceData(ServiceRequest serviceRequest) throws ServiceException;
	
	/**
	 * Ping.
	 *
	 * @param pingRequest the ping request
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String ping(String pingRequest) throws ServiceException;
}

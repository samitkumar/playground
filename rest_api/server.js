var express = require('express');
var app = express();
var path = require('path');
var fs = require('fs');
app.use(express.static(path.join(__dirname, '/')));

app.get('/test', function (req, res) {
	res.set('content-type','application/json');
	var d = require('./i18n.json')
	res.send(d);
});

app.get('/test1', function (req, res) {
	res.set('content-type','application/json');
	var d = require('./genericMessage.json')
	res.send(d);
});

/*
app.get('/white-label.css', function (req, res) {
	res.set('content-type','text/css');
	res.sendFile(path.join(__dirname + '/css/white-label.css'));
});

app.get('/maerskline.css', function (req, res) {
	res.set('content-type','text/css');
	res.sendFile(path.join(__dirname + '/css/maerskline.css'));
});
*/


app.listen(3001,function(){
	console.log('******* application is running on port 3001 *******');
})
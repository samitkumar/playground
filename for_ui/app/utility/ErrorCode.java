package utility;

public enum ErrorCode {
    ERR_CODE_1 ("critical error"),
    ERR_CODE_2 ("not found error"),
    ERR_CODE_3 ("serverside error");

    private String code;

    private ErrorCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}

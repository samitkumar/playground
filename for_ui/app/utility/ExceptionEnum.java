package utility;

public enum ExceptionEnum {
    NULL_POINTER,
    OTHER_TYPE,
    RUNTIME,
    SERVICE,
    APPLICATION_EXCEPTION,
    RUNTIME_EXCEPTION,
    EXCEPTION,
    SERVICE_EXCEPTION
}

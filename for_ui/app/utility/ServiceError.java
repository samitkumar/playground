package utility;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import play.Logger;

import java.util.*;

public class ServiceError extends RuntimeException{

    private List<ErrorCode> errorCodes = Collections.emptyList();

    public ServiceError() {
    }

    public ServiceError(List<ErrorCode> codes) {
        this.errorCodes = new ArrayList(codes);
    }

    public ServiceError(String message) {
        super(message);
    }

    public ServiceError(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceError(Throwable cause) {
        super(cause);
    }


    @Override
    public String getMessage() {
        Map<String,String> errMap = new HashMap<String, String>();
        //prepare a json string and send that back to invoker
        for(ErrorCode err : this.errorCodes){
            errMap.put(err.name(),err.getCode());
        }
        Gson gson = new GsonBuilder()
                .setPrettyPrinting().create();
        String formattedJson = gson.toJson(errMap);
        Logger.error(formattedJson);
        return formattedJson;
    }

    public List<ErrorCode> getErrorList(){
        return this.errorCodes;
    }
}

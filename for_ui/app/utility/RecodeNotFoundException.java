package utility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RecodeNotFoundException extends RuntimeException {
    List<ErrorCode> errCode = Collections.emptyList();
    public RecodeNotFoundException() {
    }

    public RecodeNotFoundException(List<ErrorCode> errCode) {
        this.errCode = new ArrayList(errCode);
    }

    public RecodeNotFoundException(String message) {
        super(message);
    }

    public RecodeNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public RecodeNotFoundException(Throwable cause) {
        super(cause);
    }

    public List<ErrorCode> getMyOwn(){
        return this.errCode;
    }

}

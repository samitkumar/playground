package utility;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ExceptionType {
    private ExceptionEnum code;
    private String exceptionType ;
    private String exceptionMessage ;
    private String exceptionDetails ;
    private Map<String,String> map = Collections.emptyMap();

    public Map<String, String> getMap() {
        return this.map;
    }

    public void setMap(Map<String, String> map) {
        this.map = new HashMap(map);
    }

    public ExceptionEnum getCode() {
        return code;
    }

    public void setCode(ExceptionEnum code) {
        this.code = code;
    }

    public String getExceptionType() {
        return exceptionType;
    }

    public void setExceptionType(String exceptionType) {
        this.exceptionType = exceptionType;
    }

    public String getExceptionMessage() {
        return exceptionMessage;
    }

    public void setExceptionMessage(String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
    }

    public String getExceptionDetails() {
        return exceptionDetails;
    }

    public void setExceptionDetails(String exceptionDetails) {
        this.exceptionDetails = exceptionDetails;
    }
}

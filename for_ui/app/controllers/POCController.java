package controllers;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import play.Logger;
import play.mvc.Catch;
import play.mvc.Controller;
import utility.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class POCController extends Controller {

    @Catch(value = Exception.class, priority = 1)
    public static void priorityException(Throwable th) {

        ExceptionType exceptionType ;

        if(th instanceof ServiceError){
            exceptionType = new ExceptionType();
            response.status=600;
            Gson gson = new GsonBuilder()
                    .setPrettyPrinting().create();
            Map<String,String> error = gson.fromJson(th.getMessage(), Map.class);
            exceptionType.setMap(error);
            ServiceError s = (ServiceError)th;
            exceptionType.setExceptionMessage(s.getErrorList().toString());
        } else if(th instanceof RecodeNotFoundException) {
            response.status=700;
            exceptionType = new ExceptionType();
            RecodeNotFoundException r = (RecodeNotFoundException)th;
            exceptionType.setExceptionMessage("");
            exceptionType.setExceptionType(r.getMyOwn().toString());
            exceptionType.setCode(ExceptionEnum.EXCEPTION);
        } else {
            th.printStackTrace();
            exceptionType = new ExceptionType();
            exceptionType.setCode(ExceptionEnum.EXCEPTION);
            exceptionType.setExceptionMessage(th.getMessage());
            response.status=500;
        }

        exceptionType.setExceptionDetails(null);

        Logger.error("error in %s ",th);
        renderJSON(exceptionType);
    }

    //except a null pointer exception
    public static void poc(){
        String x = null;
        x.replace("/","SAMIT");
        render();
    }

    //except a application error
    public static void poc1(int x) throws ApplicationException, Exception {
        if(x == 0){
            throw new Exception("value is 0");
        }else if(x > 10){
            throw new ApplicationException("value is > 10, so application error");
        } else {
         renderJSON(x);
        }
    }

    public static void poc2() throws RuntimeException{
        throw new RuntimeException("care you from poc2");
    }

    public static void serviceException(){
        List<ErrorCode> errCode = new ArrayList();
        errCode.add(ErrorCode.ERR_CODE_1);
        errCode.add(ErrorCode.ERR_CODE_2);
        throw new ServiceError(errCode);
    }
    public static void poc4(){
        List<ErrorCode> errCode = new ArrayList();
        errCode.add(ErrorCode.ERR_CODE_3);
        errCode.add(ErrorCode.ERR_CODE_2);
        throw new RecodeNotFoundException(errCode);
    }
}

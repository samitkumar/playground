package controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import play.Logger;
import play.Play;
import play.libs.IO;
import play.libs.WS;
import play.mvc.After;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.Scope;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class JsonUtil extends Controller {
    private static final String FILE_PATH = "test-result/";
    private static final String FILE_NAME = "mydaya.json";

    @Before
    public static void beforeController(){

    }

    @After
    public static void afterController(){

    }

    public void doIt(){
        WS.HttpResponse response = WS.url("http://localhost:3001/test1").get();
        if(response.getStatus()==200
                && response.getContentType().contains("application/json")){
            JsonObject element = (JsonObject)response.getJson();
            JsonElement el = element.get("data");

            //filtering the key and value with collection object
            Map<String,Object> result = new Gson().fromJson(el, Map.class);
            Map<String,Object> resultFinal = new HashMap<String, Object>();
            Set<String> keys = result.keySet();

            for(String key : keys){
                if(key.startsWith("_",0)){
                    resultFinal.put(key,result.get(key));
                }
            }
            //file write logic
            try{
                Gson gson = new GsonBuilder()
                        .setPrettyPrinting().create();
                byte[] fileTream = gson.toJson(resultFinal).getBytes();

                //PROCESS-1
                /*
                FileOutputStream fos = new FileOutputStream(FILE_PATH +FILE_NAME);
                ObjectOutputStream oos = new ObjectOutputStream(fos);
                oos.writeObject();
                oos.close();
                */

                //PROCESS-2
                IO.write(fileTream,new File(FILE_PATH+FILE_NAME));

            }catch (Exception i){
                Logger.error("IO exception !! %s ",i);
            }
            //file write logic
            renderJSON(resultFinal);
        }//if

    }

    public static void read(){
        File f = new File(FILE_PATH+FILE_NAME);
        if(f.exists()){
            renderJSON(IO.readContentAsString(f,"utf-8"));
        }else{
            renderJSON("no file found");
        }
    }

    public static void readMe(){
        renderJSON("{\"name\" : \"samit\",\"age\" : 32}");
    }

    @After
    public static void afterIntercept(){

    }
}

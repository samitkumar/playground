package controllers;

import play.mvc.Controller;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileOperation extends Controller {

    public static void getInfo(){
        File file = new File("/Users/samit/workspace");
        File[] files = file.listFiles();
        List<File> fileList = new ArrayList();
        List<File> folderList = new ArrayList();
        for(File f : files){
            if(f.isFile()){
                fileList.add(f);
            }else if(f.isDirectory()){
                folderList.add(f);
            }
        }
        render(fileList,folderList);
    }

}

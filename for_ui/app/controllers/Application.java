package controllers;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import play.Logger;
import play.Play;
import play.libs.WS;
import play.mvc.Controller;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
public class Application extends Controller {
    private static final String COLON = ":";
    private static final String NEWLINE = "\n";
    private static final String UNDERSCORE = "_";
    private static final String COMMA = ",";
    private static final String DOUBLE_QUOTE = "\"" ;
    private static final String OPEN_CARLY_BRACE = "{" ;
    private static final String CLOSE_CARLY_BRACE = "}" ;
    private static final String REQUIRE_VAR_DECLARATION = "var bundle = " + OPEN_CARLY_BRACE + NEWLINE;
    private static final String RETURN_STATEMENT = "return bundle" + ";" + NEWLINE;
    private static final String REQUIRE_JS_IMPL = "define(['messageBundler'],function()" + OPEN_CARLY_BRACE + NEWLINE ;

    public static void index() {
        render();
    }

    public static void convert() {
        try {
            WS.HttpResponse response = WS.url("http://localhost:3001/test").get();
            String jsonString = response.getString();

            ObjectMapper mapper = new ObjectMapper();

            Map<String, Object> map = new HashMap<String, Object>();

            map = mapper.readValue(jsonString, new TypeReference<Map<String, String>>() {
                //no need to override or implement anything here
            });

            StringBuilder sb = new StringBuilder();
            //append require js string
            sb.append(REQUIRE_JS_IMPL).append(REQUIRE_VAR_DECLARATION);

            createJsBasedFile(sb,map);

            System.out.println(sb);


        }catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void createJsBasedFile(StringBuilder sb, Map<String, Object> map) {
        Set<String> keys = map.keySet();
        for(String s : keys){

            String value = (String) map.get(s);
            value = DOUBLE_QUOTE + value + DOUBLE_QUOTE ;

            //append _ at the begin of the key
            s = UNDERSCORE + s;

            //replace . with _
            s = s.replace('.','_');

            // make the upper case
            s = s.toUpperCase();

            //quote the key
            s = DOUBLE_QUOTE + s + DOUBLE_QUOTE ;

            sb.append(s).append(COLON).append(value).append(COMMA).append(NEWLINE);
        }

        //close JSON

        sb.append(CLOSE_CARLY_BRACE)
                .append(NEWLINE)
                .append(RETURN_STATEMENT)
                .append(CLOSE_CARLY_BRACE);
    }


    public static void message(String iso){
        request.format = "js" ;
        File f = new File("/Users/samit/workspace/fileSystem/test.js");
        if(f.exists()){
            InputStream i;
            try {
                i = new FileInputStream(f);
                renderBinary(i,iso,"application/javascript",true);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }else {
            renderText("file missing in the mention path");
        }
    }


    public static void messageFile(){
        render();
    }

    public static void ravi_sample_page(){
        render();
    }

    public static void myForm(JsonArray body){
       Logger.info("request.contentType %s ",request.contentType);
       Logger.info("The post request is[request.param.all()] : %s",request.params.all());
       Logger.info("The post request is[String body] : %s",body);
        renderJSON(body);
    }

}
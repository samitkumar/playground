package controllers;

import play.mvc.Controller;
import play.mvc.results.Result;

import java.io.File;

/**
 * Created by samit on 26/09/16.
 */
public class ExternalAssets extends Controller {

    public static void at(String iso){
        request.format = "js";
        String staticPath = "/Users/samit/workspace/fileSystem/message";
        File f = new File(staticPath+"_"+iso+".js");
        if(f.exists()){
            renderBinary(f);
        }else{
            renderText("file not available");
        }

    }
}

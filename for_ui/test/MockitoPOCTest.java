import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;

import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import play.test.UnitTest;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class MockitoPOCTest extends UnitTest{

    @Mock
    private LinkedList mockedList;
    List list = mock(ArrayList.class);

    @Before
    public void before(){
        when(mockedList.get(0)).thenReturn(400);
        when(list.get(0)).thenReturn(500);
    }

    @Test
    public void pocTest1() {
        assertNotNull(mockedList);
        assertEquals(400,mockedList.get(0));
    }

    @Test
    public void pocTest2(){
        assertNotNull(list);
        assertEquals(500,list.get(0));
    }
}

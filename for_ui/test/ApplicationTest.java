import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.junit.*;
import play.test.*;
import play.mvc.*;
import play.mvc.Http.*;
import models.*;

public class ApplicationTest extends FunctionalTest {

    @Test
    public void testThatIndexPageWorks() {
        Response response = GET("/");
        assertIsOk(response);
        assertContentType("text/html", response);
        assertCharset(play.Play.defaultWebEncoding, response);
    }

    @Test
    public void testPostMethod(){
        String myJSON = "{\n" +
                "\t\"name\" : \"samit\",\n" +
                "\t\"roll\":\"123\",\n" +
                "  \t\"age\":\"19\"\n" +
                "  }";
        Response response = POST("/mypost","application/json",myJSON);
        assertIsOk(response);
        assertContentType("application/json",response);
        JsonParser parser = new JsonParser();
        JsonObject jsonObject = (JsonObject)parser.parse(response.out.toString());
        assertEquals("samit",jsonObject.get("name").getAsString());

    }
}
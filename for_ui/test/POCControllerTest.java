import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import groovy.ui.SystemOutputInterceptor;
import org.junit.Test;
import org.junit.runner.RunWith;
import play.mvc.Http;
import play.test.FunctionalTest;

public class POCControllerTest extends FunctionalTest {
    @Test
    public void testThatPocPageWorks() {
        Http.Response response = GET("/poc");
        assertIsOk(response);
        assertContentType("application/json", response);
        assertCharset(play.Play.defaultWebEncoding, response);
        System.out.println(response.out.toString());
        JsonParser parser = new JsonParser();
        JsonObject obj = (JsonObject)parser.parse(response.out.toString());
        System.out.println(obj.get("code"));
        System.out.println(obj.get("exceptionType"));
    }
}



requirejs.config({
    baseUrl : 'public',
    paths: {
        'jquery'        :		'thirdparty/jquery-1.12.0',
        'handlebars'    :		'thirdparty/handlebars',
        'underscore'    : 		'thirdparty/underscore',
        'backbone'      :		'thirdparty/backbone',
        'template'      :       'template/template',
        'bootstrap'     :       'bootstrap/js/bootstrap.min',
        'handlebars.runtime'   :       'thirdparty/handlebars.runtime',
        'app_partial'   :   'partials/app_partial'
    },
    shim: {
        "underscore": {
            exports: '_'
        },
        "backbone": {
            deps: ['underscore','jquery']
        },
        "bootstrap": {
            deps: ['jquery']
        },
        "javascripts/application":{
            deps : ['handlebars','backbone']
        },
        "app_partial" : {
            deps : ['handlebars.runtime','handlebars']
        },
        "template" :{
            deps : ['app_partial','handlebars.runtime']
        }
    }
});
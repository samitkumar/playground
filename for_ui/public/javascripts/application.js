define(['handlebars','backbone','handlebars.runtime'],function (Handlebars,Backbone,H) {
    var app = {};

    app.InitView = Backbone.View.extend({
        el: $('#main'),
        template: H.templates.layout(),
        initialize: function() {
            this.render();
        },
        render: function() {
            this.$el.html(this.template);
        }
    });

    new app.InitView();

    return app;
})